/**
 * Created by paras on 12/10/16.
 */

exports.route = function(req,res)
{
    fs.readFile(DIRNAME + '/metadata/adminCredentials.json', 'utf8', function (unableToOpen, data) {
        if (unableToOpen) {
            console.log('metadata/adminCredentials.json doesnt exists');
            res.send("some problem in adminCredentials.json file");
        }
        else{
            data = JSON.parse(data);
            console.log("data",data);
            res.send({
                "email":data.adminEmail,
                "password":data.adminPassword
            });
        }
    })
    
};